import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SigninPage } from './signin/signin.page';
import { SignupPage } from './signup/signup.page';


import { AuthenticationPageRoutingModule } from './authentication.routing';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    AuthenticationPageRoutingModule
  ],
  declarations: [SigninPage, SignupPage]
})
export class AuthenticationPageModule {}
