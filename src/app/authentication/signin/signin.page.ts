import { Component } from '@angular/core';

@Component({
  selector: 'app-signin',
  templateUrl: 'signin.page.html',
  styleUrls: ['signin.page.scss']
})
export class SigninPage {
  showPassword:boolean = false;
  constructor() {}

  
  togglePassword(){
    this.showPassword = !this.showPassword;
  }
}
