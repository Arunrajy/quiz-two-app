import { Component } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: 'signup.page.html',
  styleUrls: ['signup.page.scss']
})
export class SignupPage {
  showPassword:boolean = false;
  constructor() {}
  
  togglePassword(){
    this.showPassword = !this.showPassword;
  }
}
