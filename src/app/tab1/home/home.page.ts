import { Component , ViewChild} from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  slideOpts = {
    slidesPerView: 1.5,
    initialSlide: 0,
    spaceBetween: 15,
    pagination: false,
    centeredSlides: true,
    speed: 400,
    loop: true
  };
  @ViewChild('slides',{static:false}) slides:IonSlides
  constructor() {}

}
