import { Component , ViewChild, ViewEncapsulation} from '@angular/core';
import { ChartDataSets, ChartType, RadialChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfilePage {

  constructor() {}
  
  public radarChartOptions: RadialChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "#fff",
      borderColor: "#272652",
      borderWidth: 2,
      bodyFontColor:"#272652",
      footerFontFamily:'SF Text',
      footerFontColor: "#272652",
      titleFontColor:"#272652",
      titleFontSize:20,
      titleAlign: "center",
      callbacks: {
        label: function(tooltipItem:any, data:any) {
            var label = data.datasets[tooltipItem.datasetIndex].label || '';
            return label;
        }
    }
  },
    legend: {
      position : "bottom",
      labels: {
        fontColor: '#ff004e',
        fontFamily: "SF Text",
        fontSize: 16,
        padding: 10
    },
    },
    scale: {
      pointLabels:{
        fontColor: "#272652",
        fontFamily: 'SF Text',
        fontSize: 12,
        lineHeight: 4
      },
      gridLines: {
        display: true,
        color: '#ff004e',
        lineWidth:19,
      },
      angleLines:{
        display: false,
      },
      ticks: {
        display: false,
      },
    },
    
  };
  public radarChartLabels: Label[] = ['Javascript', 'HTML', 'CSS',
    'Angular', 'Bootstrap', 'Node js', 'SCSS/SASS'];

  public radarChartData: ChartDataSets[] = [
    { data: [0, 1, 2, 3, 4, 5, 6], label: 'Quiz Score' }
  ];
  public radarChartType: ChartType = 'radar';

}
