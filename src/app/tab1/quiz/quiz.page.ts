import { Component , ViewChild, ViewEncapsulation} from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: 'quiz.page.html',
  styleUrls: ['quiz.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuizPage {
  slideOpts = {
    slidesPerView: 1,
    initialSlide: 0,
    spaceBetween: 10,
    pagination: {
      el: '.slider-pagination',
      clickable: true,
      renderBullet: function (index:any, className:any) {
        return '<span class="' + className + '">' + (index + 1) + '</span>';
      },
    },
    speed: 400
  };
  hideNavigationConfirmButton:boolean;
  hideNavigationNextButton:boolean;
  @ViewChild('slides',{static:false}) slides:IonSlides
  constructor(private router : Router) {

  }
  ionViewDidEnter(){
    this.slides.lockSwipes(true);
    this.findSlideIndex();
  }
  confirmSlide(){
    console.log("completed");
    this.router.navigate(['/tabs/home/result']);
  }
  nextSlide(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.findSlideIndex();
    this.slides.lockSwipes(true);
  }
  async findSlideIndex(){
  let value1 = await this.slides.isBeginning();
  let value2 = await this.slides.isEnd();
  console.log(value1, value2)
  if(value1 == true){
    this.hideNavigationConfirmButton  = false;
    this.hideNavigationNextButton = true
  }else if(value2 == false){
    this.hideNavigationConfirmButton  = false;
    this.hideNavigationNextButton = true
  }else if(value2 == true){
    this.hideNavigationConfirmButton  = true;
    this.hideNavigationNextButton = false
  }

  }
}
