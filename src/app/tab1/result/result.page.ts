import { Component , ViewChild, ViewEncapsulation} from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-result',
  templateUrl: 'result.page.html',
  styleUrls: ['result.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResultPage {
  slideOpts = {
    slidesPerView: 1.5,
    initialSlide: 0,
    spaceBetween: 10,
    pagination: false,
    speed: 400
  };
  hideNavigationConfirmButton:boolean;
  hideNavigationNextButton:boolean;
  @ViewChild('slides',{static:false}) slides:IonSlides
  constructor() {

  }

}
