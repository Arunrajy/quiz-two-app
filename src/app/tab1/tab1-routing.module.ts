import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home/home.page';
import { QuizPage } from './quiz/quiz.page';
import { ResultPage } from './result/result.page';
import { ProfilePage } from './profile/profile.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'quiz',
    component: QuizPage
  },
  {
    path: 'result',
    component: ResultPage
  },
  {
    path: 'profile',
    component: ProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
