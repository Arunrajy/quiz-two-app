import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home/home.page';
import { QuizPage } from './quiz/quiz.page';
import { ResultPage } from './result/result.page';
import { ProfilePage } from './profile/profile.page';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ChartsModule } from 'ng2-charts';
import { Tab1PageRoutingModule } from './tab1-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Tab1PageRoutingModule,NgCircleProgressModule.forRoot({}),ChartsModule
  ],
  declarations: [HomePage, QuizPage, ResultPage, ProfilePage]
})
export class Tab1PageModule {}
