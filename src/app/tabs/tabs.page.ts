import { Component } from '@angular/core';
import { Router,NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  hideTab:boolean;
  constructor(private router:Router) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
      let url =val.url;
      let parts = url.split('/');
      console.log(parts)
      if((parts.indexOf("profile") > -1) || (parts.indexOf("quiz") > -1) || (parts.indexOf("result") > -1)){
        this.hideTab = true;
      }else{
        this.hideTab = false;
      }
    }
    })
  }

}
